﻿using System;
using System.Collections.Generic;
using System.IO;
using Novacode;
using System.Linq;
using DocMergeAPP;
using iTextSharp.text;


namespace DocMergeAPP
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"L:\Sample_Docs";
            Console.WriteLine("Process Started");
            //var doc = DocX.Load(path + @"\Research_Document.docx");
            //var doc2 = DocX.Load(path + @"\Document_CkEditor.docx");

            //////doc2.Paragraphs[0].InsertPageBreakAfterSelf();
            //doc.InsertParagraph().InsertPageBreakAfterSelf();
            //doc.InsertDocument(doc2, true);
            ////doc.Save();
            //////var doc4 = DocX.Load(path + @"\API_Gateway_Implementation_Doc.docx");
            ////var doc3 = DocX.Load(path + @"\Research_Document.docx");
            ////doc.InsertParagraph().InsertPageBreakAfterSelf();
            ////doc.InsertDocument(doc3, true);
            //doc.SaveAs(path + @"\TestMerge.docx");
            //doc.Dispose();

            List<string> arrayList = new List<string>();
            arrayList.Add(path + @"\HCFA_Test.docx");//298 KB
            arrayList.Add(path + @"\HCFA_Test1.docx");// 45 KB
            //arrayList.Add(path + @"\SAMPLE_LETTER_CSB_UB_HCFA_1.docx"); //3.05 MB
            //arrayList.Add(path + @"\Research_Document.docx"); //109 KB
            //arrayList.Add(path + @"\submain_dotnet_coding_guidelines.pdf");
            //arrayList.Add(path + @"\Asp_.Net_Core.pdf");
            //arrayList.Add(path + @"\angularjs_tutorial.pdf");
            //arrayList.Add(path + @"\submain_dotnet_coding_guidelines_pages_1.pdf");


            //string[] pdfToCombine = new string[] { path + @"\submain_dotnet_coding_guidelines.pdf", path + @"\Asp_.Net_Core.pdf", path + @"\angularjs_tutorial.pdf", path + @"\submain_dotnet_coding_guidelines_pages_1.pdf",path+ @"\SAMPLE_LETTER_CSB_UB_HCFA.pdf" };

            //string target = path + @"\FinalDemoPdf_2.pdf";
            //PDFMergeClass.CombineMultiplePDFs(pdfToCombine, target);

            PDFMergeClass.ConvertAndMerg(arrayList);
            Console.Read();


            //using (DocX documentToCreate = DocX.Load(arrayList[0]))
            //{


            //    foreach (var alItem in arrayList.Skip(1))
            //    {
            //        documentToCreate.InsertParagraph().InsertPageBreakAfterSelf();
            //        DocX documentToMergeIn = DocX.Load(alItem);
            //        //documentToCreate.InsertDocument(documentToMergeIn);
            //        File.AppendAllText(alItem, "text content" + Environment.NewLine);
            //        //documentToCreate.Save();
            //    }
            //    documentToCreate.SaveAs(path + @"\TestMerge_1.docx");

            //    Console.WriteLine("Merge Complited");



            //}
            ////var outputPath = path + @"\TestMerge.docx";

            //DocX docToCreate = DocX.Create(path + @"\TestMerge.docx");
            //foreach (var filepath in arrayList)
            //{
            //    if (arrayList.IndexOf(filepath) != 0)
            //    {
            //        docToCreate.InsertParagraph().InsertPageBreakAfterSelf();
            //    }
            //    DocX docToMerge = DocX.Load(filepath);

            //    docToCreate.InsertDocument(docToMerge);
            //}
            //docToCreate.SaveAs(outputPath);





        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Reflection;
using System.Diagnostics;


namespace DocMergeAPP
{
    public static class PDFMergeClass
    {
        public static byte[] MergeFiles(List<byte[]> sourceFiles)
        {
            Document document = new Document();
            using (MemoryStream ms = new MemoryStream())
            {
                PdfCopy copy = new PdfCopy(document, ms);
                document.Open();
                int documentPageCounter = 0;

                // Iterate through all pdf documents
                for (int fileCounter = 0; fileCounter < sourceFiles.Count; fileCounter++)
                {
                    // Create pdf reader
                    PdfReader reader = new PdfReader(sourceFiles[fileCounter]);
                    int numberOfPages = reader.NumberOfPages;

                    // Iterate through all pages
                    for (int currentPageIndex = 1; currentPageIndex <= numberOfPages; currentPageIndex++)
                    {
                        documentPageCounter++;
                        PdfImportedPage importedPage = copy.GetImportedPage(reader, currentPageIndex);
                        PdfCopy.PageStamp pageStamp = copy.CreatePageStamp(importedPage);

                        // Write header
                        ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_CENTER,
                            new Phrase("PDF Merger by Helvetic Solutions"), importedPage.Width / 2, importedPage.Height - 30,
                            importedPage.Width < importedPage.Height ? 0 : 1);

                        // Write footer
                        ColumnText.ShowTextAligned(pageStamp.GetOverContent(), Element.ALIGN_CENTER,
                            new Phrase(String.Format("Page {0}", documentPageCounter)), importedPage.Width / 2, 30,
                            importedPage.Width < importedPage.Height ? 0 : 1);

                        pageStamp.AlterContents();

                        copy.AddPage(importedPage);
                    }

                    copy.FreeReader(reader);
                    reader.Close();
                }

                document.Close();
                return ms.GetBuffer();
            }
        }

        //public static bool MergePDFs(List<string> fileNames, string targetPdf)
        //{
        //    bool merged = true;
        //    using (FileStream stream = new FileStream(targetPdf, FileMode.Create))
        //    {
        //        Document document = new Document();
        //        PdfCopy pdf = new PdfCopy(document, stream);
        //        PdfReader reader = null;
        //        try
        //        {
        //            document.Open();
        //            foreach (string file in fileNames)
        //            {
        //                reader = new PdfReader(file);
        //                pdf.AddToBody(reader);
        //                reader.Close();
        //            }
        //        }
        //        catch (Exception)
        //        {
        //            merged = false;
        //            if (reader != null)
        //            {
        //                reader.Close();
        //            }
        //        }
        //        finally
        //        {
        //            if (document != null)
        //            {
        //                document.Close();
        //            }
        //        }
        //    }
        //    return merged;
        //}


        public static void CombineMultiplePDFs(List<string> fileNames, string outFile)
        {
            // step 1: creation of a document-object
            Document document = new Document();
            //create newFileStream object which will be disposed at the end
            using (FileStream newFileStream = new FileStream(outFile, FileMode.Create))
            {
                // step 2: we create a writer that listens to the document
                PdfCopy writer = new PdfCopy(document, newFileStream);
                if (writer == null)
                {
                    return;
                }

                // step 3: we open the document
                document.Open();

                foreach (string fileName in fileNames)
                {
                    // we create a reader for a certain document
                    PdfReader reader = new PdfReader(fileName);
                    reader.ConsolidateNamedDestinations();

                    // step 4: we add content
                    for (int i = 1; i <= reader.NumberOfPages; i++)
                    {
                        PdfImportedPage page = writer.GetImportedPage(reader, i);
                        writer.AddPage(page);
                    }

                    PrAcroForm form = reader.AcroForm;
                    if (form != null)
                    {
                        writer.CopyAcroForm(reader);
                    }

                    reader.Close();
                }

                // step 5: we close the document and writer
                writer.Close();
                document.Close();
            }//disposes the newFileStream object
        }

        public static void MergeMultiplePDFs(string[] fileNames, string tagetFile)
        {

            Document document = new Document();

            using (FileStream fs = new FileStream(tagetFile, FileMode.Create))
            {
                PdfCopy writer = new PdfCopy(document, fs);
                document.Open();
                foreach (string fileName in fileNames)
                {
                    PdfReader reader = new PdfReader(fileName);
                    reader.ConsolidateNamedDestinations();
                    for (int i = 0; i < reader.NumberOfPages; i++)
                    {
                        PdfImportedPage page = writer.GetImportedPage(reader, i);
                    }


                }

            }




        }

        public static string getLibreOfficePath()
        {
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Unix:
                    return "/usr/bin/soffice";
                case PlatformID.Win32NT:
                    string binaryDirectory = @"L:\Workspace\DocMergeAPP\DocMergeAPP\Executables\program";
                    return binaryDirectory + @"\soffice.exe";
                default:
                    throw new PlatformNotSupportedException("Your OS is not supported");
            }
        }

        public static bool ConvertDocToPdf(string docFilePath, string outputDir)
        {
            string libreOfficePath = getLibreOfficePath();

            // FIXME: file name escaping: I have not idea how to do it in .NET.
            try
            {
                //soffice --headless --convert-to pdf {0} --outdir {1}
                string command = string.Format("--convert-to pdf --nologo --headless {0} --outdir {1}", docFilePath, outputDir);
                ProcessStartInfo procStartInfo = new ProcessStartInfo(libreOfficePath, command);
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                procStartInfo.CreateNoWindow = true;
                procStartInfo.WorkingDirectory = Environment.CurrentDirectory;

                Process process = new Process() { StartInfo = procStartInfo, };
                process.Start();
                process.WaitForExit();
            }
            catch (Exception ex)
            {
                //Log
                throw;
            }

            return true;

        }

        public static bool ConvertAndMerg(List<string> fileList)
        {
            Stopwatch s = new Stopwatch();
            s.Start();
            List<string> filesToMerge = new List<string>();
            string desPath = @"L:\Demos\Doc_Merge\Merg_destination";
            foreach (var item in fileList)
            {
                ConvertDocToPdf(item, desPath);
                var fileName = $"{Path.GetFileNameWithoutExtension(item)}.pdf";
                filesToMerge.Add(Path.Combine(desPath, fileName));
            }
            s.Stop();
            long convertingTime = s.ElapsedMilliseconds;
            Console.WriteLine($"Execution Time for Converting {convertingTime}");
            Stopwatch s1 = new Stopwatch();
            s1.Start();
            CombineMultiplePDFs(filesToMerge, desPath + @"\FinalMergefile_Latest.pdf");
            s1.Stop();
            Console.WriteLine($"Execution Time for Merging files {s1.ElapsedMilliseconds}");
            long mergingTime = s1.ElapsedMilliseconds;
            Console.WriteLine($"Total Execution Time is {convertingTime + mergingTime}");
            return true;
        }





    }
}

